### 1. For get all teams

**Endpoint** - /api/team/  
**Method** - GET

**Response** - 
```
[
    {
        "id": 1,
        "name": "royal club sport",
        "club_state": "first1"
    },
    {
        "id": 2,
        "name": "royal club sport",
        "club_state": "first"
    }
]
```

### 2. For create team

**Endpoint** - /api/team/  
**Method** - POST

**Parameter** -
``` 
{
    "name": "royal club sport",
    "club_state": "first2"
	
}
```


**Response** -
```
{
    "id": 3,
    "name": "royal club sport",
    "club_state": "first2"
}
```

### 3. For get team by ID

**Endpoint** - /api/team/3/   
**Method** - GET

**Response** -
``` 
{
    "id": 3,
    "name": "royal club sport",
    "club_state": "first2"
}
```


### 4. For update team by ID

**Endpoint**  - /api/team/3/  
**Method** - PUT

**Parameter** -
```
{
    "name": "royal club sport",
    "club_state": "first2"
	
}
```

**Response** -
``` 
{
    "id": 3,
    "name": "royal club sport",
    "club_state": "first2"
}
```

### 5. For get all player of given team

**Endpoint** - /api/team/1/player/   
**Method** - GET

**Response** -
```
[
    {
        "id": 1,
        "team": 1,
        "first_name": "Rock",
        "last_name": "johnson",
        "jersey_number": 11,
        "country": "India"
    },
    {
        "id": 3,
        "team": 1,
        "first_name": "Rock",
        "last_name": "johnson",
        "jersey_number": 11,
        "country": "India"
    },
]
```

### 6. For create player of given team

**Endpoint** - /api/team/1/player/  
**Method** - POST 
**Parameter** -
```
{
    "first_name": "Rock",
    "last_name": "johnson",
    "jersey_number": 11,
    "country": "India"
}
```

**Response** -
```
{
    "id": 1,
    "team": 1,
    "first_name": "Rock",
    "last_name": "johnson",
    "jersey_number": 11,
    "country": "USA"
}
```

### 7. For Get player of team

**Endpoint** - /api/team/1/player/1/  
**Method** - GET

**Response** -
```
{
    "id": 1,
    "team": 1,
    "first_name": "Rock",
    "last_name": "johnson",
    "jersey_number": 11,
    "country": "USA"
}
```

### 8. For Update player of team

**Endpoint** - /api/team/1/player/1/
**Method** - PUT

**Parameter** -
```
{
    "first_name": "Rock",
    "last_name": "johnson",
    "jersey_number": 11,
    "country": "India"
}
```

**Response** -
```
{
    "message": "Player Updated successfully"
}
```


### 9. For Delete a player of team

**Endpoint** - /api/team/1/player/1/  
**Method** - DELETE

**Response** -
```
{
    "message": "Player Deleted successfully"
}
```