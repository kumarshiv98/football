from django.db import models

# Create your models here.

class Team(models.Model):
	"""
	To store team of football player
	"""
	name = models.CharField("Name", max_length=256)
	club_state = models.CharField("Club State", max_length=256)


class Player(models.Model):
	"""
	To store team of football player
	"""
	team = models.ForeignKey(Team, verbose_name="Team", on_delete=models.CASCADE, related_name="players", blank=True, null=True)
	first_name = models.CharField("First Name", max_length=256)
	last_name = models.CharField("Last Name", max_length=256)
	jersey_number = models.IntegerField("Jersey Number", default=0)
	country = models.CharField("Country", max_length=256)