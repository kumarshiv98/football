from django.contrib import admin

from .models import Team, Player


@admin.register(Team)
class TeamAdmin(admin.ModelAdmin):
    list_display = ("name", "club_state")


@admin.register(Player)
class PlayerAdmin(admin.ModelAdmin):
    list_display = ("team", "first_name", "last_name", "jersey_number", "country")