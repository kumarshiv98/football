from django.shortcuts import render

from rest_framework import viewsets, status
from rest_framework.response import Response
from rest_framework.decorators import action

from .models import Team, Player
from .serializers import TeamSerializer, PlayerSerializer


class TeamSet(viewsets.ModelViewSet):
    """
    CRUD operation for Team Model.
    """
    queryset = Team.objects.all()
    serializer_class = TeamSerializer

    @action(detail=True, methods=['get', 'post'])
    def player(self, request, pk=None):
        team = Team.objects.filter(pk=pk).first()
        if team:
            if request.method == 'POST':
                serializer = PlayerSerializer(data=request.data)
                if serializer.is_valid():
                    serializer.save()
                    serializer.instance.team = team
                    serializer.instance.save()
                    return Response(serializer.data)
                return Response(serializer.errors)

            else:
                players = team.players.all()
                data = PlayerSerializer(players, many=True).data
                return Response(data)

    @action(detail=True, methods=['get', 'delete', 'put'], url_path='player/(?P<player_id>\d+)',
            url_name='project-related-needs')
    def player_detail(self, request, player_id, pk=None):
        team = Team.objects.filter(pk=pk).first()
        if team:
            player = team.players.filter(pk=player_id).first()
            if player:
                if request.method == "DELETE":
                    player.delete()
                    return Response({"message" : "Player deleted successfully"})
                elif request.method == "PUT":
                    serializer = PlayerSerializer(data=request.data)
                    if serializer.is_valid():
                        serializer.save()
                        return Response({"message": "Player Updated successfully"})
                    return Response(serializer.errors)
                data = PlayerSerializer(player).data
                return Response(data)
            return Response({"message": "Player not found"})
        return Response({"message": "Team not found"})